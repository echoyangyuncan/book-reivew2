

# Register your models here.
from django.contrib import admin
from books.models import Book
from books.models import Magazine, Author, BookReview, Genre, Issue_number



class BookAdmin(admin.ModelAdmin):
    pass


# Register your models here.

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Author)
admin.site.register(BookReview)
admin.site.register(Genre)
admin.site.register(Issue_number)
