from django import forms
from .models import Book 
from .models import Magazine

class GeeksForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "title",
            "authors",
            "book_page",
            "in_print",
            "url_for_bookcover",
            "description",
        ]
class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "title",
            "release_cycle",
            "description",
            "cover_image",
        ]