from django.db import models

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class  Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name




# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=125)
    authors = models.ManyToManyField(Author, blank=True)
    book_page = models.IntegerField(null=True)
    url_for_bookcover = models.URLField(null=True, blank=True)
    isbn_number = models. IntegerField(null=True, blank=False)
    in_print = models.BooleanField(null=True)
    published_date = models.DateField(auto_now=True)
    description = models.TextField(null=True)
   

    def __str__(self):
        return self.title + " by " + str(self.authors.first())

class Magazine(models.Model):
    title = models.CharField(max_length = 200)
    published_date = models.DateField(auto_now=True)
    release_cycle = models.CharField(max_length=100)
    description = models.TextField(null=True)
    cover_image = models.URLField(null=True, blank=True)
    genres = models.ManyToManyField(Genre,blank=True)

    

class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

class  Issue_number(models.Model):
    book = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    text = models.TextField()