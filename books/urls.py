from django.contrib import admin
from django.urls import path
from books.views import (
    magazine_detail,
    show_books,
    create_view,
    book_detail,
    edit_book,
    delete_book,
    show_genre_detail,
    show_magazines,
    magazine_detail,
    magazine_create_view,
    edit_magazine,
    delete_magazine,
    show_genres,
    show_issues,
    show_genre_detail,
)

urlpatterns = [
    path("", show_books,name ="show_books"),
    path("create/", create_view, name="create_view"),
    path("<int:pk>/", book_detail, name="book_detail"),
    path("<int:pk>/edit/", edit_book, name="edit_book"),
    path('<int:pk>/delete/', delete_book, name ="delete_book" ),
    path("magazines/", show_magazines, name="show_magazines"),
    path("magazines/<int:pk>/", magazine_detail, name="magazine_detail"),
    path("magazines/create/", magazine_create_view, name="magazine_create_view"),
    path("magazines/<int:pk>/edit/", edit_magazine, name="edit_magazine"),
    path("magazines/<int:pk>/delete/", delete_magazine, name="delete_magazine" ),
    path("magazines/genres/", show_genres, name="show_genres"),
     path("magazines/issues/", show_issues, name="show_issues"),
    path("magazines/genres/<int:pk>/", show_genre_detail, name="show_genre_detail"),
    
]
