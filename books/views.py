from multiprocessing import context
from django.shortcuts import redirect, render
from .forms import GeeksForm, MagazineForm
from books.models import Book, Magazine,Author,Genre,BookReview,Issue_number


# Create your views here.
def show_books(request):
    books = Book.objects.all()
    context ={
        "books": books
    }
    return render(request, "books/list.html",context)

def create_view(request):
    context ={}
    form = GeeksForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")
          
    context['form']= form
    return render(request, "books/create_view.html", context)

def book_detail(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
    }
    return render(request, "books/detail.html", context)

def edit_book(request, pk):
    # if Book and GeeksForm:
    instance = Book.objects.get(pk=pk)
    if request.method == "POST":
        form = GeeksForm(request.POST, instance=instance)
        if form.is_valid():
                form.save()
                return redirect("show_books")
    #     else:
    form = GeeksForm(instance=instance)
    # else:
    #     form = None
    context = {
        "form": form,
    }
    return render(request, "books/edit.html", context)

def delete_book(request,pk):
    if request.method =="POST":
        instance = Book.objects.get(pk=pk)
        instance.delete()
        return redirect("show_books")
    return render(request, "books/delete_book.html")

def show_magazines(request):
    magazines = Magazine.objects.all()
    context ={
        "magazines": magazines
    }
    return render(request, "magazines/mlist.html",context)

def magazine_detail(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, "magazines/mdetail.html", context)

def magazine_create_view(request):
    context ={}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_magazines")
          
    context['form']= form
    return render(request, "magazines/mcreate_view.html", context)

def edit_magazine(request, pk):
    # if Book and GeeksForm:
    instance = Magazine.objects.get(pk=pk)
    if request.method == "POST":
        form = MagazineForm(request.POST, instance=instance)
        if form.is_valid():
                form.save()
                return redirect("show_magazines")
    #     else:
    form = MagazineForm(instance=instance)
    # else:
    #     form = None
    context = {
        "form": form,
    }
    return render(request, "magazines/medit.html", context)

def delete_magazine(request,pk):
    if request.method =="POST":
        instance = Magazine.objects.get(pk=pk)
        instance.delete()
        return redirect("show_magazines")
    return render(request, "magazines/delete_magazine.html")

def show_genres(request):
    genres= Genre.objects.all()
    context ={  "genres": genres
    }
    return render(request, "magazines/genres.html",context)
    

def show_genre_detail(request,pk):
    magazines = Magazine.objects.filter(genres__id = pk).all()
    genre = Genre.objects.get(pk=pk)
    context ={
        "genre": genre,
        "magzines": magazines
    }
    return render(request, "magazines/genre_detail.html",context)

def show_issues(request):
    issues= Issue_number.objects.all()
    context ={  "issues": issues
    }
    return render(request, "magazines/issues.html",context)
